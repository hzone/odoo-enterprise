# Odoo Enterprise :office:

Bienvenue dans le dépôt GitLab du projet **Odoo Enterprise**, qui est conçu pour faciliter le déploiement et la gestion d'Odoo 17 en version Enterprise.

## :memo: À propos

Ce projet contient deux branches principales :
- `main`: Contient le script `install.sh` pour l'installation des prérequis.
- `enterprise-addons`: Contient les modules complémentaires en version entreprise pour Odoo 17.

## :wrench: Prérequis

Pour exécuter ce projet, assurez-vous que votre environnement de serveur répond aux exigences suivantes :
- **Système d'exploitation** : Ubuntu 22.04
- **Mémoire RAM** : 4 GB
- **Processeur** : 2 CPU
- **Espace disque** : Minimum 32 GB

## :arrow_down: Installation

Pour installer Odoo et configurer les addons enterprise, suivez ces étapes :

### 1. Clonage du dépôt
Clonez le dépôt sur votre machine locale ou serveur avec la commande suivante :
```bash
git clone https://thegit.dev/wettayeb/odoo-enterprise.git
```

### 2. Installation des prérequis
Naviguez dans la branche `main` et exécutez le script `install.sh` :
```bash
cd odoo-enterprise
git checkout main
./install.sh
```

## :question: Support

Si vous avez des questions ou des problèmes concernant ce dépôt, n'hésitez pas à soumettre une issue ici sur GitLab.

