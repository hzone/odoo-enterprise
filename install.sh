#!/bin/bash

# Définition des variables
ODOO_KEY_URL="https://nightly.odoo.com/odoo.key"
ODOO_GPG_KEY="/usr/share/keyrings/odoo-archive-keyring.gpg"
ODOO_REPO_URL="https://nightly.odoo.com"
ODOO_VERSION="17.0" 
ODOO_REPO_DEB="$ODOO_REPO_URL/$ODOO_VERSION/nightly/deb/"
ODOO_LIST_FILE="/etc/apt/sources.list.d/odoo.list"
GIT_REPO_URL="https://thegit.dev/wettayeb/odoo-enterprise.git"
GIT_ODOO_URL_INDUSTRIES="https://github.com/odoo/industry.git"
CLONE_DIR="/mnt/enterprise-addons"
CLONE_DIR_INDUSTRIES="/mnt/industry-addons"
BRANCH_NAME="enterprise-addons"

# Installation des dépendances
sudo apt install -y postgresql unzip git wkhtmltopdf

# Configuration de la clé Odoo
wget -q -O - $ODOO_KEY_URL | sudo gpg --dearmor -o $ODOO_GPG_KEY

# Ajout du dépôt Odoo
echo "deb [signed-by=$ODOO_GPG_KEY] $ODOO_REPO_DEB ./" | sudo tee $ODOO_LIST_FILE

# Mise à jour des paquets et installation d'Odoo
sudo apt-get update && sudo apt-get install -y odoo

# Ajout des addons enterprise
sudo git clone --branch $BRANCH_NAME $GIT_REPO_URL $CLONE_DIR
sudo git clone --branch $ODOO_VERSION $GIT_ODOO_URL_INDUSTRIES $CLONE_DIR_INDUSTRIES
# Changement du propriétaire du dossier
sudo chown -R odoo:odoo $CLONE_DIR

echo 'addons_path = /usr/lib/python3/dist-packages/odoo/addons,/mnt/enterprise-addons,/mnt/industry-addons' >> /etc/odoo/odoo.conf

sudo systemctl restart odoo.service
echo "Installation terminée avec succès!"
