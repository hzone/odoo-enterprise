#!/bin/bash

# Définition des variables
GIT_REPO_URL="https://thegit.dev/wettayeb/odoo-enterprise.git"
CLONE_DIR="/mnt/enterprise-addons"
BRANCH_NAME="enterprise-addons"
CLONE_DIR_INDUSTRIES="/mnt/industry-addons"
GIT_ODOO_URL_INDUSTRIES="https://github.com/odoo/industry.git"
ODOO_VERSION="17.0" 


sudo apt update && sudo apt dist-upgrade -y 
sudo rm -rf $CLONE_DIR
sudo rm -rf $CLONE_DIR_INDUSTRIES
sudo git clone --branch $BRANCH_NAME $GIT_REPO_URL $CLONE_DIR
sudo git clone --branch $ODOO_VERSION $GIT_ODOO_URL_INDUSTRIES $CLONE_DIR_INDUSTRIES

sudo chown -R odoo:odoo $CLONE_DIR
sudo chown -R odoo:odoo $CLONE_DIR_INDUSTRIES

sudo systemctl restart odoo
